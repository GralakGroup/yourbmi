package com.android.szkolenie.bmi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Wynik extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wynik);
        double BMI = getIntent().getDoubleExtra("BMI", 0);
        EditText displaymessage = (EditText) findViewById(R.id.Wynikkoncowy);
        displaymessage.setText(String.format("BMI = %.2f ", BMI));
    }

    public void Zamknij(View view) {
        finish();
    }


}
