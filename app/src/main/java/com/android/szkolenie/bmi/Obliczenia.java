package com.android.szkolenie.bmi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Obliczenia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obliczenia);
    }

    public void Wynik(View view) {
        Intent podajwynik = new Intent(Obliczenia.this, Wynik.class);
        podajwynik.putExtra("BMI", onClick());
        startActivity(podajwynik);
        finish();

    }

    public double onClick() {


        DecimalFormat nf = new DecimalFormat("0.00");


        EditText wzrostUrzytkowika = (EditText) findViewById(R.id.Wzrost);


        EditText wagaUrzytkowniaka = (EditText) findViewById(R.id.Waga);


        double height = Double.parseDouble(wzrostUrzytkowika.getText().toString()) / 100;


        double weight = Double.parseDouble(wagaUrzytkowniaka.getText().toString());


        double BMI = weight / (height * height);


        return BMI;


    }
}
