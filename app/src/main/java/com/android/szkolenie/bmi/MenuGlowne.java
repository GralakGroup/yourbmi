package com.android.szkolenie.bmi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuGlowne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_glowne);

    }

    public void Oblicz(View view) {
        Intent przejscie = new Intent(MenuGlowne.this, Obliczenia.class);
        startActivity(przejscie);
    }

    public void Zamknij(View view) {
        System.exit(0);
    }

}
