package com.android.szkolenie.bmi;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Starter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);

        // Odtwarzanie muzyki podczas startu

        MediaPlayer player = MediaPlayer.create(this, R.raw.starter);


        // Watek który czekaa 5 sekund i przechodzi do kolejnego Activity

        Thread przejscie = new Thread() {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(Starter.this, MenuGlowne.class);
                    startActivity(intent);


                }
            }

        };
        przejscie.start();

    }
}
